DELETE FROM `usuarioscriticasdb`.`users_has_authorities`;
DELETE FROM `usuarioscriticasdb`.`Authorities`;
ALTER TABLE `usuarioscriticasdb`.`Authorities` AUTO_INCREMENT = 1 ;

DELETE FROM `usuarioscriticasdb`.`Users`;
ALTER TABLE `usuarioscriticasdb`.`Users` AUTO_INCREMENT = 1 ;

INSERT INTO `usuarioscriticasdb`.`Users` VALUES (1, 'Admin', 'Admin', 'admin@uah.es', 1);


INSERT INTO `usuarioscriticasdb`.`Authorities` VALUES (1, 'ROLE_ADMIN');
INSERT INTO `usuarioscriticasdb`.`Authorities` VALUES (2, 'ROLE_USER');

INSERT INTO `usuarioscriticasdb`.`users_has_authorities` VALUES (1, 1);