package uah.es.usuariocriticas.dao;

import uah.es.usuariocriticas.model.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRolesJPA extends JpaRepository<Rol, Integer> {
}
