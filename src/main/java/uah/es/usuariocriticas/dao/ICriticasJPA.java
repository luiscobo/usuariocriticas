package uah.es.usuariocriticas.dao;

import uah.es.usuariocriticas.model.Critica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ICriticasJPA extends JpaRepository<Critica, Integer> {

    List<Critica> findByIdPelicula(int idPelicula);

    //List<Critica> findByUsuario(Usuario usuario);

}
