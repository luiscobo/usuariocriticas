package uah.es.usuariocriticas.dao;

import uah.es.usuariocriticas.model.Critica;

import java.util.List;

public interface ICriticasDAO {

    List<Critica> buscarTodas();

    List<Critica> buscarCriticasPorIdPelicula(Integer idPelicula);

    //List<Critica> buscarCriticasPorUsuario(Usuario usuario);

    Critica buscarCriticaPorId(Integer idCritica);

    void guardarCritica(Critica critica);

    void eliminarCritica(Integer idCritica);

}
