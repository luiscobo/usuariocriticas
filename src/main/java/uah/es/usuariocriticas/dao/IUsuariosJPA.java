package uah.es.usuariocriticas.dao;

import uah.es.usuariocriticas.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsuariosJPA extends JpaRepository<Usuario, Integer> {

    Usuario findByNombre(String nombre);

    Usuario findByCorreo(String correo);

    Usuario findByCorreoAndClave(String correo, String clave);

}
