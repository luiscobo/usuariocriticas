package uah.es.usuariocriticas.dao;

import uah.es.usuariocriticas.model.Rol;

import java.util.List;

public interface IRolesDAO {

    List<Rol> buscarTodos();

    Rol buscarRolPorId(Integer idRol);

    void guardarRol(Rol rol);

    void eliminarRol(Integer idRol);

}
