package uah.es.usuariocriticas.controller;

import uah.es.usuariocriticas.model.Critica;
import uah.es.usuariocriticas.model.Usuario;
import uah.es.usuariocriticas.service.ICriticasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uah.es.usuariocriticas.service.IUsuariosService;

import java.util.List;

@RestController
public class CriticasController {

    @Autowired
    ICriticasService criticasService;

    @Autowired
    IUsuariosService usuariosService;

    @GetMapping("/criticas")
    public List<Critica> buscarTodas() {
        return criticasService.buscarTodas();
    }

    @GetMapping("/criticas/{id}")
    public Critica buscarCriticaPorId(@PathVariable("id") Integer id) {
        return criticasService.buscarCriticaPorId(id);
    }

    @GetMapping("/criticas/pelicula/{idPelicula}")
    public List<Critica> buscarCriticasPorIdPelicula(@PathVariable("idPelicula") Integer idPelicula) {
        return criticasService.buscarCriticasPorIdPelicula(idPelicula);
    }

    @GetMapping("/criticas/usuario/{idUsuario}")
    public List<Critica> buscarCriticasPorUsuario(@PathVariable("idUsuario") Integer idUsuario) {
        Usuario usuario = usuariosService.buscarUsuarioPorId(idUsuario);
        return usuario.getCriticas();
    }

    @PostMapping("/criticas")
    public void guardarCritica(@RequestBody Critica critica) {
        System.out.println(critica.getIdCritica());
        criticasService.guardarCritica(critica);
    }

    @DeleteMapping("/criticas/{id}")
    public void eliminarCritica(@PathVariable("id") Integer id) {
        criticasService.eliminarCritica(id);
    }

}
