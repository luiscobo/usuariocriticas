package uah.es.usuariocriticas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsuariocriticasApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsuariocriticasApplication.class, args);
    }

}
